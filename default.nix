let
  pkgs = (import <nixos-unstable> { }).pkgsCross.aarch64-multiplatform;
  ubootDerivations = pkgs.callPackage ./uboot-quartz64.nix { };
in
{
  ubootSoQuartz = ubootDerivations.ubootSoQuartz;
  ubootQuartz64a = ubootDerivations.ubootQuartz64a;
  ubootQuartz64b = ubootDerivations.ubootQuartz64b;
}
