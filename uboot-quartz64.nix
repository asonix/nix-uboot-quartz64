{ lib
, fetchFromGitHub
, buildUBoot
}:

let
  bl31Blobs = fetchFromGitHub {
    owner = "JeffyCN";
    repo = "rockchip_mirrors";
    rev = "6186debcac95553f6b311cee10669e12c9c9963d";
    sha256 = "nH/g95QMv0bFgbT5jiKTAa3Vd0gnqtG4M6nkZ5R3S0E=";
  };
  rkbinBlobs = fetchFromGitHub {
    owner = "rockchip-linux";
    repo = "rkbin";
    rev = "a4c6de9ea29f275bb1d08c94ccded51ff2ab5b92";
    sha256 = "e3RH/4hi+xc5cQpzt2txyZYNMGQvC1jGDJpzBY2QSHo=";
  };
  buildQuartz64UBoot = (defconfig: buildUBoot {
    version = "2022.64-rc1";
    defconfig = defconfig;
    src = fetchFromGitHub {
      owner = "CounterPillow";
      repo = "u-boot-quartz64";
      rev = "87c317f72735f9b336c55b9e00222e71816040e7";
      sha256 = "mmy6fY6+7aVSdYr/pZCX2kTP7/Zj3Ib1uBN9gFWFoxw=";
    };
    extraMakeFlags = [
      "ARCH=arm"
    ];
    extraMeta = {
      platforms = [ "aarch64-linux" ];
      license = lib.licenses.unfreeRedistributableFirmware;
    };
    filesToInstall = [ "u-boot.itb" "idbloader.img" ];
    preConfigure = ''
      make mrproper

      cp "${bl31Blobs}/bin/rk35/rk3568_bl31_v1.28.elf" "bl31.elf"
      cp "${rkbinBlobs}/bin/rk35/rk3566_ddr_1056MHz_v1.13.bin" ram_init.bin
    '';
  });
in
{
  ubootSoQuartz = buildQuartz64UBoot "soquartz-rk3566_defconfig";
  ubootQuartz64b = buildQuartz64UBoot "quartz64-b-rk3566_defconfig";
  ubootQuartz64a = buildQuartz64UBoot "quartz64-a-rk3566_defconfig";
}
